---
title: 信息系统项目管理师中高级常见英语词汇
date: 2021-03-12 09:23:21
tags:
- pmp
categories:
- [项目管理,英语]
---

## 信息系统项目管理师中高级常见英语词汇

<!-- more -->

|        |       |
|  ----  | ----  |
| Project  | 项目 |
| Monte Carlo Analysis | 蒙特卡洛分析 |
| Temporary | 临时性 |
| Sensitivity Analysis | 灵敏度分析 |
| Unique | 独特的 |
| Threepoint Estimate | 三点估算 |
| Product | 产品 |
| Pareto Chart | 帕累托图 |
| Service | 服务 |
| Reserve Analysis | 预留分析 |
| Result  | 成果 |
| COCOMOConstructive Cost Model | 构造性成本模型 |
| Project management | 项目管理 |
| ADMArrow Diagram Method | 箭线图法 |
| Project manager | 项目经理 |
| PDMPrecedence Diagram Method | 前导图法 |
| Project requirements | 项目需求 |
| Bottom Up Estimating | 自底向上法 |
| accomplished | 通过 |
| Decision Tree Analysis | 决策树分析 |
| Initiating | 启动 |
| AOAActive On the Arrow | 双代号网络图法 |
| Planning | 计划 |
| Critical Design Review | 关键设计评审 |
| Executing | 实施 |
| Workaround | 权变措施 |
| Monitoring | 监视 |
| S-CurveS | 曲线 |
| Controlling | 控制 |
| Schedule | 进度 |
| Closing | 收尾 |
| Schedule Analysis | 进度计划分析 |
| Project Charter | 项目章程 |
| Schedule Compression | 进度计划压缩 |
| Project Management Plan | 项目管理计划 |
| Schedule Control | 进度计划控制 |
| Phase | 阶段 |
| Dummy Activity | 虚活动 |
| Direct | 指导 |
| Optimistic time | 乐观时间 |
| Manage | 管理 |
| Most likely time | 最可能时间 |
| Monitor | 监视 |
| Pessimistic time | 悲观时间 |
| Control | 控制 |
| ESEarliest Start Time | 最早开始时间 |
| Change | 变更 |
| EFEarliest Finish Time | 最早完成时间 |
| Reviewing | 评审 |
| LSLatest Start Time | 最迟开始时间 |
| Change requests | 变更请求 |
| LFLatest Finish Time | 最迟完成时间 |
| Deliverables | 可交付物、可交付成果 |
| TFTotal Float | 总时差 |
| Organizational process assets | 组织过程资产 |
| FFFree Float | 自由时差 |
| Close | 关闭、收尾 |
| Crashing | 压缩、赶工 |
| Time Management | 时间管理即进度管理 |
| Concurrent Engineering | 并行工程 |
| Activity Definition | 活动定义 |
| Resource Calendar | 资源日历 |
| Activity Sequencing | 活动排序 |
| Resource Leveling | 资源平衡 |
| Activity Resource Estimating | 活动资源估算 |
| Resource Planning | 资源规划 |
| Activity Duration Estimating | 活动持续时间估算 |
| Fast Tracking | 快速跟进 |
| Schedule Development | 进度计划 |
| Product Scope | 产品范围 |
| Schedule Control | 进度控制 |
| Project Scope | 项目范围 |
| Cost Management | 成本管理 |
| Scope Change | 范围变更 |
| Cost Planning | 成本计划 |
| Scope Creep | 范围蔓延 |
| Cost Estimating | 成本估算 |
| Scope Definition | 范围定义 |
| Cost Budgeting | 成本预算 |
| Scope Verification | 范围验证 |
| Cost Controlling | 成本控制 |
| Work Package | 工作包 |
| Quality Management | 质量管理 |
| Cost | 成本 |
| Quality Planning | 质量规划 |
| ABCActivity Based Costing | 基于活动的成本核算 |
| Quality Assurance | 质量保证 |
| EVMEarned Value Management | 挣值管理 |
| Quality Control | 质量控制 |
| PVPlan Value | 计划工作量的预算费用 |
| Human Resource Management | 人力资源管理 |
| ACActual Cost | 已完成工作量的实际费用 |
| Organize | 组织 |
| EVEarned Value | 已完成工作量的预算成本 |
| Acquire | 组建 |
| SVSchedule Variance | 进度偏差 |
| Develop | 建设、发展 |
| CVCost Variance | 费用偏差 |
| Communications Management | 沟通管理 |
| CPICost Performed Index | 成本绩效指标 |
| Collection | 收集 |
| SPISchedul Performed Index | 进度绩效指标 |
| Dissemination | 传输 |
| EACEstimate At Completion | 完成时估算 |
| Storage | 存储 |
| BACBudget At Completion | 计划总额 |
| Disposition | 处理 |
| ETCEstimate To Complete | 完成尚需成本估算 |
| Communications Planning | 沟通规划 |
| Cost Estimating | 成本估算 |
| Information Distribution | 信息发布 |
| Cost Management Plan | 成本管理计划 |
| Performance Reporting | 绩效报告 |
| Cost Baseline | 成本基准 |
| Stakeholders | 项目干系人、利害相关者 |
| Cost Budget | 成本预算 |
| Risk | 风险 |
| Cost Variance | 成本偏差 |
| Risk Management | 风险管理 |
| Cost of Quality | 质量成本 |
| Risk Management Planning | 风险管理规划 |
| Quality | 质量 |
| Risk Identification | 风险识别 |
| TQMTotal Quality Management | 全面质量管理 |
| Qualitative Risk Analysis | 定性风险分析 |
| QAQuality Assurance | 质量保证 |
| Quantitative Risk Analysis | 定量风险分析 |
| QCQuality Control | 质量控制 |
| Risk Response Planning | 风险应对计划 |
| Acceptable Quality Level | 可接受质量水平 |
| Risk Monitoring and Control | 风险监控 |
| Deliverable | 可交付物 |
| Procurement Management | 采购管理 |
| Benchmarking Analysis | 基准比较分析法 |
| Plan Purchases and Acquisitions | 采购规划 |
| OBSOrganizational Breakdown Structure | 组织分解结构 |
| Plan Contracting | 合同计划 |
| Select Sellers | 卖方选择 |
| RBSResource Breakdown Structure | 资源分解结构 |
| Contract Administration | 合同管理 |
| Contract Closure | 合同收尾 |
| RAMResponsibility Assignment Matrix | 责任分配矩阵 |
| Projects | 项目 |
| PMBOKProject Management Body Of Knowledge | 项目管理知识体系 |
| Virtual Team | 虚拟团队 |
| Team Development | 团队建设 |
| Operations | 运作 |
| Team members | 团队成员 |
| Process | 过程 |
| Communicate | 沟通 |
| Activity | 活动 |
| Communication Channel | 沟通渠道 |
| Activity Description | 活动描述 |
| Communication Plan | 沟通计划 |
| Activity Definition | 活动定义 |
| Information Distribution | 信息分发 |
| Activity Description | 活动描述/说明 |
| Performance Report | 绩效报告 |
| Activity List | 活动清单 |
| Problem Solving | 问题解决 |
| Phases | 阶段 |
| Compromise | 妥协 |
| Approve | 批准 |
| Smooth | 圆滑 |
| Product Life Cycle | 产品生命周期 |
| Force | 强迫 |
| PMOProject Management Office | 项目管理办公室 |
| Withdrawal | 撤退 |
| Project Charter | 项目章程 |
| Risk | 风险 |
| Project Manager | 项目经理 |
| Risk Distinguish | 风险识别 |
| Project Sponsor | 项目发起人 |
| Risk Analysis | 风险分析 |
| Project Stakeholder | 项目干系人 |
| Quantitative Risk Analysis | 定量风险分析 |
| Project Management Plan | 项目管理计划 |
| Qualitative Risk Analysis | 定性风险分析 |
| Project Team | 项目团队 |
| Risk Response | 风险应对 |
| Functional Organization | 职能组织 |
| Risk Acceptance | 风险接受 |
| Matrix Organization | 矩阵型组织 |
| Risk Aversion | 风险规避 |
| Project Organization | 项目型组织 |
| Risk Mitigation | 风险缓解 |
| PMISProject Management Information System | 项目管理信息系统 |
| Residual Transference | 风险转移 |
| Residual Risk | 残余风险 |
| Project Management Process Group | 项目管理过程组 |
| CMConfiguration Management | 配置管理 |
| CCBConfiguration Management Board | 配置管理委员会 |
| Initiating Process | 启动过程组 |
| Planning Process | 计划过程组 |
| CMOConfiguration Management Officer | 配置管理员 |
| Executing Process | 执行过程组 |
| Controlling Process | 控制过程组 |
| CIConfiguration Items | 配置项 |
| Closing Process | 收尾过程组 |
| Version | 版本 |
| Plan | 计划 |
| Document | 文档 |
| Rolling Wave Plan | 滚动式计划 |
| System Documentation | 系统文档 |
| Do | 行动 |
| User Documentation | 用户文档 |
| Check | 检查 |
| Product Documentation | 产品文档 |
| Action | 处理 |
| Configuration Library | 配置库 |
| Walkthrough | 走查 |
| Development Library | 开发库 |
| Inspection | 审查 |
| Controlled Library | 受控库 |
| Review | 评审 |
| Product Library | 产品库 |
| Demonstration | 论证 |
| Base line | 基线 |
| Roundrobin Review | 轮查 |
| Milestone | 里程碑 |
| Defect | 缺陷 |
| Check point | 检查点 |
| Brainstorming | 头脑风暴法 |
| Configuration Status Report | 配置状态报告 |
| CMMCapability Maturity Model | 能力成熟度模型 |
| Outsourcing | 外包 |
| CMMICapability Maturity Model Integration | 能力成熟度模型集成 |
| APRAcquisition Plan Review | 采购计划评审 |
| Strategy | 战略 |
| Input | 输入 |
| SWOTStrengths | 优势 |
| Weaknesses | 劣势
| Output | 输出 |
| Opportunities | 机遇 |
| Threats | 挑战 |
| Tool | 工具 |
| Supervisor | 监理 |
| Method | 方法 |
| Checklist | 检查单 |
| Technology | 技术 |
| RFPRequest for Proposal | 请求建议书 |
| Enterprise Environmental Factors | 事业环境因素 |
| RFQRequest for Quotation | 请求报价单 |
| Organizational Process Assets | 组织过程资产 |
| Contract | 合同 |
| SOWStatement Of Work | 工作说明书 |
| Contract Administration | 合同管理 |
| CRChange Request | 变更请求 |
| Contract Closeout | 合同收尾 |
| CCBChange Control Board | 变更控制委员会 |
| Contract Target Cost | 合同目标成本 |
| WBSWork Breakdown Structure | 工作分解结构 |
| CPFFCost Plus Fixed Fee | 成本加固定费用（合同） |
| Delphi | 德尔菲法 |
| CPMCritical Path Method| 关键路线法 |
| CPIFCost Plus Incentive Fee| 成本加奖励费用（合同） |
| Gantt Chart | 甘特图 |
| Bar Chart | 横道图 |
| FFPFirm Fixed Price | 完全固定总价（合同） |
| PERTProgram Evaluation and Review Technique | 计划评审技术 |
| Discounted Cash Flow | 折现现金流 |
| Claim | 索赔 |
| Graphical Evaluation and Review Technique | 图形评审技术 |
| Accept | 验收 |
| Acceptance Standard | 验收标准 |
| Analogous Estimating | 类比估算 |
| Expert Judgment | 专家判断 |

### 项目管理基本概念英语例句

A project is a temporary endeavor undertaken to create a unique product, service, or result.

译文：项目是为提供某项独特产品、服务或成果所做的临时性努力。

Project management is the application of knowledge, skills, tools and techniques to project activities to meet project requirements. Project management is accomplished through the application and integration of the project management processes of initiating, planning, executing, monitoring and controlling, and closing. The project manager is the person responsible for accomplishing the project objectives.

译文：项目管理就是把各种知识、技能、手段和技术应用于项目活动之中，以实现项目需求。项目管理是通过应用和综合诸如启动、计划、实施、监控和收尾等项目管理过程来进行的。项目经理是负责实现项目目标的个人。

### 各管理过程及输入、输出、工具和方法等英语知识

Develop Project Charter – developing the project charter that formally authorizes a project or a project phase.  

译文：制定项目章程：制定项目章程，正式授权一个项目或一个项目阶段。

Develop Project Management Plan – documenting the actions necessary to define, prepare, integrate, and coordinate all subsidiary plans into a project management plan.  

译文：制定项目管理计划：将定义、准备、集成所需的各项活动文档化，协调优化各分计划，以形成整体项目管理计划。

Direct and Manage Project Execution – executing the work defined in the project management plan to achieve the project’s requirements defined in the project scope statement.

译文：指导和管理项目执行：执行在项目管理计划中所定义的工作以达到项目的目标。

Monitor and Control Project Work – monitoring and controlling the processes used to initiate, plan, execute, and close a project to meet the performance objectives defined in the project management plan. 

译文：监视和控制项目工作：监测和控制的过程，用来启动，计划，执行和结束项目，以满足项目管理计划中定义的性能目标。

Integrated Change Control – reviewing all change requests, approving changes, and controlling changes to the deliverables and organizational process assets.  

译文：整体变更控制：评审所有的变更请求，批准变更，控制可交付成果和组织的过程资产。

Close Project – finalizing all activities across all of the Project Management Process Groups to formally close the project or a project phase. 

译文： 项目收尾：完成项目过程中的所有活动，以正式结束一个项目或项目阶段。

#### 时间管理部分

Project Time Management, describes the processes concerning the timely completion of the project. It consists of the Activity Definition, Activity Sequencing, Activity Resource Estimating, Activity Duration Estimating, Schedule Development, and Schedule Control project management processes.

译文：项目时间管理，描述确保项目按时完成所需的各项过程，包括活动定义、活动排序、活动资源估算、活动持续时间估算、进度计划以及进度控制等项目管理过程。

#### 成本管理部分

Project Cost Management, describes the processes involved in planning, estimating, budgeting, and controlling costs so that the project is completed within the approved budget. It consists of the Cost Estimating, Cost Budgeting, and Cost Control project management processes.

译文：项目成本管理，描述确保项目按照规定预算完成须进行的成本规划、估算、预算的各项过程。项目成本管理由如下项目管理过程组成：成本估算、成本预算和成本控制。

#### 质量管理部分

Project Quality Management, describes the processes involved in assuring that the project will satisfy the objectives for which it was undertaken. It consists of the Quality Planning, Perform Quality Assurance, and Perform Quality Control project management processes.

译文：项目质量管理，描述为确保项目达到其既定质量要求所需实施的各项过程。项目质量管理由如下项目管理过程组成：质量规划、实施质量保证和实施质量控制。

#### 人力资源管理部分

Project Human Resource Management, describes the processes that organize and manage the project team. It consists of the Human Resource Planning, Acquire Project Team, Develop Project Team, and Manage Project Team project management processes.

译文：项目人力资源管理，描述组织和管理项目团队的各个过程。项目人力资源管理由如下项目管理过程组成：人力资源规划、团队组建、团队建设和项目团队管理。

#### 沟通管理部分

Project Communications Management, describes the processes concerning the timely and appropriate generation, collection, dissemination, storage and ultimate disposition of project information. It consists of the Communications Planning, Information Distribution, Performance Reporting, and Manage Stakeholders project management processes.

译文：项目沟通管理，描述为确保项目信息及时且恰当地提取、收集、传输、存储和最终处置而需要实施的一系列过程。项目沟通管理由如下项目管理过程组成：沟通规划、信息发布、绩效报告和项目干系人管理。

#### 风险管理部分

Project Risk Management, describes the processes concerned with conducting risk management on a project. It consists of the Risk Management Planning, Risk Identification, Qualitative Risk Analysis, Quantitative Risk Analysis, Risk Response Planning, and Risk Monitoring and Control project management processes.

译文：项目风险管理，描述与项目风险管理有关的过程。项目风险管理由如下项目管理过程组成：风险管理规划、风险识别、定性风险分析、定量风险分析、风险应对计划，以及风险监控。

#### 采购管理部分

Project Procurement Management , describes the processes that purchase or acquire products, services or results, as well as contract management processes. It consists of the Plan Purchases and Acquisitions, Plan Contracting, Select Sellers, Contract Administration, and Contract Closure project  management processes.

译文：项目采购管理，描述采办或取得产品、服务或成果，以及合同管理所需的各过程。目采购管理由如下项目管理过程组成：采购规划、合同计划、卖方选择、合同管理以及合同收尾。 
