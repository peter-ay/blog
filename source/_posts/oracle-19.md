---
title: oracle临时表空间
date: 2021-02-09 18:14:05
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 用来存放用户的临时数据，临时数据就是在需要时被覆盖，关闭数据库后自动删除，其中不能存放永久性数据
* 例如用户对很多数据进行排序时，排序在PGA中进行，但是如果排序的数据过多，导致内存不足时，oracle会把要排序的数据分成多份，每次只取一份放在PGA中进行排序，其他部分放在临时表空间，当PGA中部分进行排序完成后，把排序好的部分交换到临时表空间中，同时再从临时表空间里取一份没有排序的数据到PGA中进行排序，这样指定所有数据排序完成为止。

## 临时表空间组

* 临时表空间组是一组由临时表空间组成的组，临时表空间组和临时表空间不能同名。临时表空间组不能显式地创建和删除；当把第一个临时表空间分配给某个临时表空间组时，会自动创建这个临时表空间组；将临时表空间组的最后一个临时表空间删除时，会自动删除临时表空间组。

![1](oracle-19/1.png)

<!-- more -->

![2](oracle-19/2.png)

![3](oracle-19/3.png)

![4](oracle-19/4.png)

![5](oracle-19/5.png)

查看临时表空间组

![6](oracle-19/6.png)

查看默认的临时表空间

```sql
select * from database_properties t1 where t1.property_name='DEFAULT_TEMP_TABLESPACE';
```

![7](oracle-19/7.png)

```sql
--创建临时表空间
create TEMPORARY TABLESPACE temp2 TEMPFILE '/home/oracle/app/oracle/oradata/helowin/temp02.dbf' size 10m AUTOEXTEND on;
--指定组
create TEMPORARY TABLESPACE temp3 TEMPFILE '/home/oracle/app/oracle/oradata/helowin/temp03.dbf' size 10m AUTOEXTEND on TABLESPACE GROUP tbs_grp_01;
--把临时表空间加入组
alter TABLESPACE temp2 TABLESPACE GROUP tbs_grp_01;
--把临时表空间移出组
alter TABLESPACE temp2 TABLESPACE GROUP '';
--给表空间添加临时文件
alter TABLESPACE temp2 add TEMPFILE '/home/oracle/app/oracle/oradata/helowin/temp04.dbf' size 10m AUTOEXTEND on;
--修改系统默认临时表空间
alter DATABASE DEFAULT TEMPORARY TABLESPACE tbs_grp_01;
alter DATABASE DEFAULT TEMPORARY TABLESPACE temp2;
```
