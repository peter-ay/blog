---
title: oracle的dual表
date: 2020-05-06 15:55:59
tags: oracle 12c
categories: 
- [读书笔记,oracle]
---



[官方说明](https://docs.oracle.com/database/121/SQLRF/queries009.htm#SQLRF20036)
> DUAL is a table automatically created by Oracle Database along with the data dictionary. DUAL is in the schema of the user SYS but is accessible by the name DUAL to all users. It has one column, DUMMY, defined to be VARCHAR2(1), and contains one row with a value X. Selecting from the DUAL table is useful for computing a constant expression with the SELECT statement. Because DUAL has only one row, the constant is returned only once. Alternatively, you can select a constant, pseudocolumn, or expression from any table, but the value will be returned as many times as there are rows in the table. 


DUAL 是一个实际存在的虚表，任何用户均可读取，存在的目的是作为 from的源。
<!-- more -->
[引用网上一些例子](https://www.cnblogs.com/qiangqiang/archive/2010/10/15/1852229.html)

```sql

1. 查看当前用户 select user from dual;

2. 用来调用系统函数

　　select to_char(sysdate,'yyyy-mm-dd hh24:mi:ss') from dual;--获得当前系统时间

　　select SYS_CONTEXT('USERENV','TERMINAL') from dual;--获得主机名

　　select SYS_CONTEXT('USERENV','language') from dual;--获得当前 locale

　　select dbms_random.random from dual;--获得一个随机数

3. 得到序列的下一个值或当前值，用下面语句

　　select your_sequence.nextval from dual;--获得序列your_sequence的下一个值

　　select your_sequence.currval from dual;--获得序列your_sequence的当前值

4. 可以用做计算器 select 7*9 from dual;

```

首先搞清楚DUAL是什么OBJECT :

```sql
select owner, object_name , object_type from dba_objects where object_name like '%DUAL%';
```

![1](./oracle-1/1.png)

原来DUAL是属于SYS schema的一个表,然后以PUBLIC SYNONYM的方式供其他数据库USER使用.

再看看它的结构:

![2](./oracle-1/2.png)

DUAL表可以执行插入、更新、删除操作，也执行drop操作。但是不要去执行drop表的操作，否则会使系统不能用会报Database startup crashes with ORA-1092错误。

如果DUAL表被“不幸”删除后的恢复：
用sys用户登陆。
创建DUAL表。
授予公众SELECT权限（SQL如上述，但不要给UPDATE，INSERT，DELETE权限）。
向DUAL表插入一条记录（仅此一条）： insert into dual values(''X'');
提交修改。
具体操作：

用sys用户登陆。
```sql
SQL> create pfile=’d:/pfile.bak’ from spfile
SQL> shutdown immediate
--在d:/pfile.bak文件中最后加入一条：replication_dependency_tracking = FALSE
--重新启动数据库：
SQL> startup pfile=’d:/pfile.bak’
SQL> create table “sys”.”DUAL”
( “DUMMY” varchar2(1) )
pctfree 10 pctused 4;
SQL> insert into dual values(‘X’);
SQL> commit;
SQL> Grant select on dual to Public;
--授权成功。
```


