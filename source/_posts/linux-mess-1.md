---
title: sftp限制用户访问固定目录
date: 2020-03-27 00:07:27
tags: 
- sftp
- linux
categories:
- [linux,sftp]
---

今天想临时简单搭个ftp给普通用户上传文件到服务器，又实在懒得配vsftp，甚至docker都懒得跑，于是想到sftp，记录一下全程：

## 准备上传下载的目录

```shell
mkdir -p /data/ftp/pub
```

上传下载目录的上一级目录ftp一定要root用户,而且权限是755.

```sh
chown root:root /data/ftp
chmod 775 /data/ftp
```
<!-- more -->
## 添加专门用sftp的用户

```sh
useradd u1 -d /data/ftp/pub -s /sbin/nologin
echo '123456' |passwd --stdin u1
```

## 设置上传目录的拥有者

这时需要设置专门使用ftp目录的拥有者为新建的用户

```sh
chown u1:u1 /data/ftp/pub
```

## 修改ssh配置

```sh
# vi /etc/ssh/sshd_config
#Subsystem sftp /usr/libexec/openssh/sftp-server

Subsystem sftp internal-sftp
Match User u1   #ftp的用户名
      ChrootDirectory /data/ftp  #限制ftp用户的目录
      AllowTCPForwarding no
      X11Forwarding no
      ForceCommand internal-sftp
```

重启ssh

```ssh
systemctl restart sshd
```

测试:

```sh
sftp u1@192.168.1.123
```

***

## 总结

* 还是用docker方便。

***
