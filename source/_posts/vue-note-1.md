---
title: vue定时器销毁
date: 2020-04-28 13:22:09
tags:
 - Vue
 - 前端技巧
categories: 
- [Vue]
---

## 方案1:

```js
data() {            
    return {                              
        timer: null  // 定时器名称          
    }        
},
this.timer = (() => {
    // 某些操作
}, 1000)
beforeDestroy() {
    clearInterval(this.timer);        
    this.timer = null;
}
```

## 方案2:

```js
const timer = setInterval(() =>{                    
    // 某些定时器操作                
}, 500);            
// 通过$once来监听定时器，在beforeDestroy钩子可以被清除。
this.$once('hook:beforeDestroy', () => {            
    clearInterval(timer);                                    
})
```

参考文章[vue组件里定时器销毁问题](https://www.cnblogs.com/ycg-myblog/p/10353680.html)
