---
title: 日常口语2-包在我身上
date: 2020-03-20 10:12:31
tags:
- english
- daily conversation
categories:
- [英语学习,日常会话]
---

## “包在我身上” consider it done/Your wish is my command

***
>A: I'm sorry,but I can't go to the movies with you tonight.Because I hava so many chores and must run a few errands.
>很抱歉，我今天晚上不能配你看电影，因为我有很多家务要做，还必须跑几个地方办事。
>
>B:That's okay,I understand. What do you need to do?
>没关系，我理解，你需要做什么呢？
><!-- more -->
>A: I need to pick something up for dinner on the way home. Then I must prepare the school lunches for my two sisters. Oh, I'm so busy.
>我的在回家的路上买做晚餐的东西，然后我得替两个妹妹准备学校的午餐。啊，我好忙。
>
>B:Maybe I can come over and help out?
>我或许可以过来帮一把。
>
>A:Really? Could you? I need some dry cleaning picked up.
>真的吗？你可以吗？我需要有人去拿干洗衣服。
>
>B:Consider it done,Anything else?
>包在我身上，还有别的事吗？
>
>A:Could you hang out the washing?
>你可以把洗好的衣服拿去外面凉吗？
>
>B:Not a problem.
>没问题。
>
>A:And wash the dishes?
>还可以洗碗盘吗？
>
>B:Just call me the human dishwasher.
>叫我人肉洗碗机就好。
>
>A:Great,maybe I will have time to see a movie tonight.
>太棒了，今晚我或许有空可以看电影了。
