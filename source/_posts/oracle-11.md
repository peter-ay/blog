---
title: oracle视图
date: 2021-01-27 23:23:08
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 经过定制的方式显示来自一个或多个表的数据
* 视图可以视为“虚拟表”或“存储空间”
* 视图可以使用单行函数、分组函数和表达式，但必须指定名字
  
## 优点

* 提供另一种级别的安全性
* 隐藏的数据复杂性
* 简化用户的SQL命令
* 隔离基表结构的改变
* 通过重命名列，从另一个角度提供数据

<!-- more -->

## 语法

```sql
CREATE [OR REPLACE] [FORCE] VIEW
  view_name [(alias[,alias]...)]
AS select_statement
[WITH CHECK OPTION]
[WITH READ ONLY];
```

* 加了WITH CHECK OPTION，则oracle会阻止视图更新符合where条件中的行
* 加了WITH READ ONLY，则不能通过视图更新数据

## 视图的DML语句

* 视图可以使用INSERT，UPDATE，DELETE等DML语句
* 只能修改一个底层的基表
* 修改不能违反基表约束
* 如果视图包含连接操作符，DISTINCT，集合操作符，聚合函数，或者group by子句，则无法更新视图
* 如果视图包含伪列或者表达式，则无法更新视图

## 键保留表

* 主键保留到视图的表，oracle允许更新键保留表
* 可以使用instead of 触发器更新复杂视图

## 删除视图

```sql
DROP VIEW view_name
```

## 查看所有视图

```sql
select * from user_views;
```
