---
title: oracle UNDO表空间
date: 2021-02-09 19:02:42
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 对于DML来说，只要修改了数据块，Oracle数据库就会将修改前的数据保留下来，保存在undo segment里，而undo segment则保存在undo表空间。
* 对于undo的管理，有手工undo和自动undo，11g默认使用自动undo管理

## undo作用

* 当Oracle在提动一致读的过程中，会跟进ITL槽里记录的undo块的底座，而搜索使用相关联的undo块，只要使用undo块的事务提交或者回滚，则这些undo块就不再被事务所需要，也就是说其中包含旧的数据可以被覆盖，这样就会发生在查找改变前的旧值过程中，找不到足够旧的值，而爆出ORA-1555的错误消息，为此，oracle为undo管理提供了另一个参数：undo_retention,改参数以秒为单位，表示当事务提交或者回滚以后，改事务所使用的undo块里的数据需要保留多长时间，当保留时间超过undo_retention所指定的时间以后，该undo块才能够被其他事务覆盖。
* 当没有undo块可以使用，如果设置了rentention garentee,则数据库报错，否则把之前的数据覆盖掉。不管retention参数

<!-- more -->

```sql
--查看undo表空间
select * from dba_tablespaces t1;
--select * from dba_data_files t1;
--查看管理方式
show parameter undo_management;
--增加回滚表空间
create undo TABLESPACE undotbs2 DATAFILE '/home/oracle/app/oracle/oradata/helowin/undotbs02.dbf' SIZE 10m AUTOEXTEND ON MAXSIZE UNLIMITED;
--增加数据文件
alter TABLESPACE undotbs2 add DATAFILE '/home/oracle/app/oracle/oradata/helowin/undotbs03.dbf' SIZE 10m;
--切换undo表空间
alter system set undo_tablespace=undotbs2;
show PARAMETER undo
-- 启用guarantee
alter TABLESPACE undotbs2 RETENTION GUARANTEE;
--查看
select t1.tablespace_name,t1.retention from dba_tablespaces t1;
```

## v$undostat

* 每10分钟更新一次，记录了undo表空间的使用情况

![1](oracle-20/1.png)
