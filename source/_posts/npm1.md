---
title: npm使用国内淘宝镜像的方法
date: 2020-03-18 15:55:59
tags: npm
categories: 
- 日常备忘
---

## 通过命令配置

```shell
npm config set registry https://registry.npm.taobao.org
```

## 查看

```shell
npm config ls -l
```
