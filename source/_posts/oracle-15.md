---
title: oracle日志
date: 2021-02-02 13:35:06
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* ORacle数据库有联机重做日志，这个日志是记录数据库所做的修改，包括对表作的数据改变，对系统做的改变等，可以使用它来维护数据的完整性，以及对数据库的恢复，日志挖掘
* 日志文件分为在线日志文件和归档日志文件。归档日志是在线日志的历史备份
* LGWR 把日志缓冲区的重做条目写到联机日志文件中，redo01.log，redo02.log，redo03.log...
* 数据库启动的时候，有前滚，回滚，只要用户有commit的记录，都不会丢失，只要是用户没有commit的记录，都不会保存
* 日志按照组来组织，每个组里面有多个文件。日志按照循环的方式来工作。所以在oracle里面至少有两个日志组，当一个联机重做日志组被写满的时候，就会发生日志切换，这时联机重做日志组2成为当前使用的日志，当联机重做日志2写满的时候，又会发生日志切换，去写日志组1，反复进行
* 如果数据库处于非归档模式，联机日志在切换时就会丢弃，而在归档模式下，日志切换的时候，切换的日志会进行归档。比如，当重做日志1写满的时候，发生日志切换，开始写日志2，这时联机重做日志1的内容会拷贝到另外一个指定的目录下。这个目录叫归档目录，拷贝的文件叫归档重做日志。
* 数据库使用归档方式运行时才可以进行灾难性恢复

<!-- more -->

## 归档方式

* 非归档日志方式可以避免实例故障，但无法避免介质故障。在此种方式下，数据库只能冷备份
* 归档日志方式产生归档日志，用户可以使用归档日志完全恢复数据库

![1](oracle-15/1.png)

* 配置归档日志方式

![2](oracle-15/2.png)

```sql
--改变非归档到归档模式
shutdown immediate;
startup mount;
alter database archivelog;
alter database open;
```

![3](oracle-15/3.png)

```sql
--切换日志
alter system switch logfile;
```

![4](oracle-15/4.png)

```sql
--设置多个归档目录
alter system set log_archive_dest_1='/app/oracle_my_data/';
```

* 由于联机日志的重要性，应该以组的方式建立日志文件，数据库中最少有两个日志文件组，同时每个日志文件组至少包含两个日志文件，每一个日志组里面所有的日志成员内容完全相同

```sql
--添加日志文件
alter database add logfile member '/u04/app/oracle/redo/redo009.log' to group 3;
```

![5](oracle-15/5.png)

```sql
--添加日志组
--同一组内的大小必须一样，所以添加日志文件不需要指定大小
alter database add logfile group 4 '/u04/app/oracle/redo/redo04.log' size 100M;
```

![6](oracle-15/6.png)

## 日志4个状态

* 从v$logfile查看日志状态
* current:正在使用的联机日志文件组
* active：表示这个日志文件组仲，所记录的重做记录所对应的内存中的脏数据块还没有完全写入到数据文件中
* inactive：表示这个日志文件组中，所记录的重做记录中的脏数据块已经被写入数据文件中
* unused：表示还没有被使用过
* 使用alter database clear logfile group (id) 清除组内所有文件。必须是inactive状态

## 自动归档和手动归档

* 手动 alter system switch logfile;
* v$archive_dest 显示当前归档日志存储位置及状态
* v$archived_log 显示历史归档信息

![7](oracle-15/7.png)
