---
title: 日常口语3-指引方向
date: 2020-03-21 17:12:58
tags:
- english
- daily conversation
categories:
- [英语学习,日常会话]
---
## point sb in the direction of..(指引某人去...的方向）

>Could you point me in the direction of the library?

## be around the corner(在附近)

>The shop where I often shop is around the corner.
***
{% blockquote %}
A:Excuse me,I'm lost. Could you point me in the direction of Bridge Street,please?
不好意思，我迷路了，可以麻烦你告诉我去大桥街大路吗？
<!-- more -->
B:Sure,it's not too far from here.In fact,it's just around the conner.
没问题，那里不远，事实上就在附近。

A:You're a lifesaver. I've been driving in circles for ages.
你真是救命恩人，我已经开车饶了很久。

B:You should've made a left-hand turn a few streets back.Do a U-turn and take the third street on the right.It's called Gary street.There's a gas station on the corner,you cann't miss it.
你应该在几条街之前左转，你先回转，接着在第三条街口右转，那条街叫Gray街，有个加油站在转角处，很好找。

A:U turn..third on the right.
回转，，，第三条街右转。

B:Make you way alone Gray street all the way to the end.It ends at Bridge Street.
沿着格雷街一直走下去，走到底结尾就是大桥街。

A:All the way to the end.Thanks a million,I really appreciate it.
走到底，太感谢你了，我真的非常感激。

B:Glad to help.Hava a great day.
很开心能帮上忙。祝你有美好大一天。

{% endblockquote %}
