---
title: oracle物化视图
date: 2021-02-19 19:45:39
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 在分布式海量的数据环境中，信息查询的速度问题尤为重要。传统的查询方式，即根据用户的要求，每次都重新进入基本或视图的查询，所需要的时间较长，例如移动通信行业，即使客户需要查询很少的信息，也可能会花费很多时间，可能在30分钟左右，如果数据库主机繁忙，这个时间会更久，客户难以忍受，为了解决这个问题oracle设计了物化视图。又称为MV.materialized view

* 物化视图是包括一个查询结果的数据库对象，它是远程数据的本地副本，或者用来生成基于数据泵求和的汇总表。物化视图存储基于远程表的数据，也可以成为快照。物化视图可以查询表，视图和其他物化视图。通常情况下，无话视图被称为主表（在复制期间）或明细表（在数据仓库中）

* 提高查询速度，是数据库性能调整的重点。

1. 调整sga和pga
2. 调整sql语句，使用绑定变量
3. 使用物化视图
4. 系统结构上使用读写分离

* 对于复制，物化视图允许你在本地维护远程数据的副本，这些副本是只读的，如果你想修改本地副本，必须用高级复制的功能。当你想从一个表或是条钟抽取数据时，可以从物化视图中抽取。对于数据仓库，创建的物化视图通常情况下是聚合视图，单一表聚合视图和连接视图。
* 物化视图有很多方面和索引很相似：使用物化视图的目的是为了提高查询性能；物化视图对于应用透明，增加和删除物化视图不会影响应用程序中的SQL语句的正确性和有效性，物化视图需要占用存储空间，当基表发生变化时，物化视图也应当刷新

<!-- more -->

## 作用

1. 物化视图和表一样可以直接进行查询，物化视图可以基于分区表，物化视图本身也可以分区
2. 数据仓库中的物化视图主要用于预先计算并保存表链接或聚集等耗时交多操作的结果，这样，在执行查询时，就可以避免进行这些耗时的操作，而快速的得到结果。在数据仓库中，还经常使用查询重写(query rewrite)机制，这样不需要修改原有的查询语句，Oracle会自动选择合适的物化视图进行查询，完全对应用透明
3. 除了在数据仓库使用，物化视图还用于复制，移动，计算方面。

### 选项

1. 查询重写(Query Rewrite),是指当对物化视图的基表进行查询时，Oracle会自动判断能否通过查询物化视图来得到结果。默认为DISABLE QUERY REWRITE
2. 物化视图日志：如果需要进行快速刷新，侧需要建立物化视图日志，物化视图视图日志根据不同的物化视图快速刷新需要，可以建立为ROWID或PRIMARY KEY类型的
3. 刷新(Refresh): 指当基表发生DML操作后，物化视图何时采用哪种方式和基表进行同步，刷新的模式有两种：ON DEMAND和ON COMMIT.ON DEMAND指在用户需要的时候进行刷新，可以手工通过DBMS_MVIEW.REFRESH等方式进行刷新，也可以通过HJOB定时进行刷新。ON COMMIT指对基表的DML操作进行提交的同时进行刷新，熟悉的方式有FAST，COMPLETE，FOECE和NEVER。FAST刷新采用增量刷新，只刷新上次刷新以后进行的修改。COMPLETE最整个物化视图进行完全的刷新。FORCE方式ORACLE判断是否可以进行快速刷新，如可以则FAST否则COMPLETE.NEVER指物化视图不进行任何刷新。默认值是FORCE ON DEMAND

### 使用条件

* 远程表必须包含主键
* 远端操作：创建主键表，并创建基于主表的视图日志
* 本地操作：创建一个过程，实现刷新本地物化视图的动作，创建一个JOB，进行调用过程的操作。运行这个JOB，则本地数据会与远程数据自动同步刷新一次
* 如果创建基于主键的物化视图，则必须具有访问表，访问主表日志，create materialized view这三个权限
  
```sql
--remote server
CREATE table stu(id varchar2(10) PRIMARY key,name VARCHAR2(10));
create MATERIALIZED view log on stu;
grant SELECT on stu to hr;
grant select on MLOG$_STU to hr;
--local database
create MATERIALIZED view stu_view 
REFRESH fast 
start with sysdate
next sysdate+1/1440
with PRIMARY key
as 
select * from scott.stu;

```

```sql
--
create or replace PROCEDURE test as 
begin
 dbms_mview.refresh('STU_VIEW');
end;

variable job1 number;
begin
 dbms_job.submit(: job1,'test',sysdate,'sysdate+1/1440');
end;

begin
 dbms_job.run(: job1);
end;
```

* 如果创建基于rowid的物化视图，则必须具有访问表，create materialized view这两个权限
