---
title: oracle的水位线
date: 2020-05-25 09:01:15
tags: oracle 12c
categories: 
- [读书笔记,oracle]
---

## ORACLE在逻辑存储上分4个粒度:表空间,段,区,块.

1. 块:是粒度最小的存储单位,现在标准的块大小是8K,ORACLE每一次I/O操作也是按块来操作的,也就是说当ORACLE从数据文件读数据时,是读取多少个块,而不是多少行. 

2. 区:由一系列相邻的块而组成,这也是ORACLE空间分配的基本单位,举个例子来说,当我们创建一个表PM_USER时,首先ORACLE会分配一区的空间给这个表,随着不断的INSERT数据到PM_USER,原来的这个区容不下插入的数据时,ORACLE是以区为单位进行扩展的,也就是说再分配多少个区给PM_USER,而不是多少个块.  

3. 段:是由一系列的区所组成,一般来说,当创建一个对象时(表,索引),就会分配一个段给这个对象.所以从某种意义上来说,段就是某种特定的数据.如CREATE TABLE PM_USER,这个段就是数据段,而CREATE INDEX ON PM_USER(NAME),ORACLE同样会分配一个段给这个索引,但这是一个索引段了.查询段的信息可以通过数据字典: SELECT * FROM USER_SEGMENTS来获得,

4. 表空间:包含段,区及块.表空间的数据物理上储存在其所在的数据文件中.一个数据库至少要有一个表空间.

OK,我们现在回到HWM上来,那么,什么是高水位标记呢?这就跟ORACLE的段空间管理相关了.

<!-- more -->
## ORACLE用HWM来界定一个段中使用的块和未使用的块.

举个例子来说,当我们创建一个表:PT_SCHE_DETAIL时,ORACLE就会为这个对象分配一个段.在这个段中,即使我们未插入任何记录,也至少有一个区被分配,第一个区的第一个块就称为段头(SEGMENT HEADE),段头中就储存了一些信息,基中HWM的信息就存储在此.此时,因为第一个区的第一块用于存储段头的一些信息,虽然没有存储任何实际的记录,但也算是被使用,此时HWM是位于第2个块.当我们不断插入数据到PM_USER后,第1个块已经放不下后面新插入的数据,此时,ORACLE将高水位之上的块用于存储新增数据,同时,HWM本身也向上移.也就是说,当我们不断插入数据时,HWM会往不断上移,这样,在HWM之下的,就表示使用过的块,HWM之上的就表示已分配但从未使用过的块.

## HWM在插入数据时,当现有空间不足而进行空间的扩展时会向上移,但删除数据时不会往下移

这就好比是水库的水位,当涨水时,水位往上移,当水退出后,最高水位的痕迹还是清淅可见.  
考虑让我们看一个段，如一张表，其中填满了块，如图 1 所示。  
![1](./oracle-2/1.gif)  
图1：分配给该表的块。用灰色正方形表示行

在正常操作过程中，删除了一些行，如图 2 所示。 
![1](./oracle-2/2.gif)  
图2：行后面的块已经删除了；HWM 仍保持不变

现有就有了许多浪费的空间：
- 在表的上一个末端和现有的块之间，以及 
- 在块内部，其中还有一些没有删除的行。

## HWM的信息存储在段头当中.

HWM本身的信息是储存在段头.在段空间是手工管理方式时,ORACLE是通过FREELIST(一个单向链表)来管理段内的空间分配.在段空间是自动管理方式时(ASSM),ORACLE是通过BITMAP来管理段内的空间分配.
 
## ORACLE的全表扫描是读取高水位标记(HWM)以下的所有块.
 
所以问题就产生了:
- 当用户发出一个全表扫描时，ORACLE 始终必须从段一直扫描到 HWM，即使它什么也没有发现。该任务延长了全表扫描的时间。
- 当用直接路径插入行时 — 例如，通过直接加载插入（用 APPEND 提示插入）或通过 SQL*LOADER 直接路径 — 数据块直接置于 HWM 之上。它下面的空间就浪费掉了。

我们来分析这两个问题,后者只是带来空间的浪费,但前者不仅是空间的浪费,而且会带来严重的性能问题.我们来看看下面的例子:  

1.  我们先来搭建测试的环境,第一步先创建一个段空间为手工管理的表空间:

```sql
CREATE TABLESPACE "TESTHWN" 
LOGGING 
DATAFILE '/u01/data/testhwm.dbf' SIZE 5M 
AUTOEXTEND ON NEXT 10M MAXSIZE UNLIMITED 
EXTENT MANAGEMENT LOCAL 
SEGMENT SPACE MANAGEMENT MANUAL;
```
2. 创建一个表,注意,此表的第二个字段设成是CHAR(100),以让此表在插入1千万条记录后,空间有足够大:


```sql
CREATE TABLE TEST_T1(C1 NUMBER(10),C2 CHAR(100)) TABLESPACE TESTHWM;
```
插入记录
```sql
DECLARE
    I NUMBER(10);
BEGIN
    FOR I IN 1..10000000 LOOP
        INSERT INTO TEST_T1 VALUES(I,'TESTSTRING123');
    END LOOP;
    COMMIT;
END;
```

1. 我们来查询一下,看在插入一千万条记录后所访问的块数和查询所用时间:


```sql
SQL> SET TIMING ON
SQL> SET AUTOTRACE TRACEONLY
SQL> SELECT COUNT(*) FROM TEST_T1;
```
```sql
Elapsed: 00:00:00.26

Execution Plan
----------------------------------------------------------
Plan hash value: 2778628632

-----------------------------------------------------------------------
| Id  | Operation	   | Name     | Rows  | Cost (%CPU)| Time     |
-----------------------------------------------------------------------
|   0 | SELECT STATEMENT   |	      |     1 | 42321	(1)| 00:00:02 |
|   1 |  SORT AGGREGATE    |	      |     1 | 	   |	      |
|   2 |   TABLE ACCESS FULL| TEST_T1  |    10M| 42321	(1)| 00:00:02 |
-----------------------------------------------------------------------


Statistics
----------------------------------------------------------
	18  recursive calls
	 0  db block gets
156050  consistent gets
156012  physical reads
     0  redo size
   542  bytes sent via SQL*Net to client
   551  bytes received via SQL*Net from client
     2  SQL*Net roundtrips to/from client
     3  sorts (memory)
     0  sorts (disk)
     1  rows processed
```

上面的执行计划,逻辑读了156050个BLOCK,物理读了156012个BLOCK. 

```sql
select t1.blocks,t1.num_rows,t1.empty_blocks from user_tables t1 where table_name = 'TEST_T1';
-- 156012	10000000	0
```

4. 把此表的记录用DELETE方式删掉,然后再来看看SELECT COUNT(*) FROM TEST_T1: 

```sql
DELETE FROM TEST_T1; 
COMMIT;
SELECT COUNT(*) FROM TEST_T1;
```
```sql
Elapsed: 00:00:00.48

Execution Plan
----------------------------------------------------------
Plan hash value: 2778628632

-----------------------------------------------------------------------
| Id  | Operation	   | Name     | Rows  | Cost (%CPU)| Time     |
-----------------------------------------------------------------------
|   0 | SELECT STATEMENT   |	      |     1 | 42321	(1)| 00:00:02 |
|   1 |  SORT AGGREGATE    |	      |     1 | 	   |	      |
|   2 |   TABLE ACCESS FULL| TEST_T1 |    10M| 42321	(1)| 00:00:02 |
-----------------------------------------------------------------------


Statistics
----------------------------------------------------------
	  1  recursive calls
	  0  db block gets
     312027  consistent gets
     156012  physical reads
	  0  redo size
	541  bytes sent via SQL*Net to client
	551  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  1  rows processed
```

5. 用truncate


```sql
truncate table test_t1;
```
```sql
Elapsed: 00:00:00.02

Execution Plan
----------------------------------------------------------
Plan hash value: 2778628632

-----------------------------------------------------------------------
| Id  | Operation	   | Name     | Rows  | Cost (%CPU)| Time     |
-----------------------------------------------------------------------
|   0 | SELECT STATEMENT   |	      |     1 | 42321	(1)| 00:00:02 |
|   1 |  SORT AGGREGATE    |	      |     1 | 	   |	      |
|   2 |   TABLE ACCESS FULL| TEST_T1 |    10M| 42321	(1)| 00:00:02 |
-----------------------------------------------------------------------


Statistics
----------------------------------------------------------
	  3  recursive calls
	  1  db block gets
	 10  consistent gets
	  0  physical reads
	104  redo size
	541  bytes sent via SQL*Net to client
	551  bytes received via SQL*Net from client
	  2  SQL*Net roundtrips to/from client
	  0  sorts (memory)
	  0  sorts (disk)
	  1  rows processed

```

[参考:Oracle高水位线(HWM)及性能优化](https://www.cnblogs.com/husam/p/6604437.html)







