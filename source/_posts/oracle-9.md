---
title: oracle同义词
date: 2021-01-26 14:36:26
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 同义词是先有对象的一个别名
  * 简化SQL语句
  * 隐藏对象名称的所有者
  * 提供对对象的所有访问

### 私有同义词

* 只能在其模式内访问，且不能与当前模式的对象重名
* 需要create synonym权限

```sql
grant create synonym to scott;
```

```sql
create synonym for s1 for scott.emp;
```
<!-- more -->
### 公有同义词

* 被所有的数据库用户访问
* 需要create public synonym权限

```sql
grant create synonym to scott;
```

```sql
create public synonym for s1 for emp;
```

## 删除同义词

* drop public synonym权限

```sql
grant drop public synonym to scott;
```

```sql
drop public synonym s1;
```

## 查看同义词

![1](oracle-9/1.png)

![2](oracle-9/2.png)
