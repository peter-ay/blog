---
title: oracle数据库恢复练习
date: 2020-06-06 14:56:28
tags: oracle 12c
categories: 
- [读书笔记,oracle]
---

## 数据库关闭时的恢复

当数据库未打开，执行完全恢复时，可以在一个操作中恢复所有损坏的数据文件，或者分别在单个操作对 每一个损坏的数据文件进行单独恢复。

该恢复过程假设:
* 当前控制文件可用;
* 所有的数据文件都已备份;
* 所有需要的归档重做日志可用
```sql
SELECT 'cp '|| NAME ||' /home/oracle/backup' cp_name FROM v$datafile 
UNION ALL
SELECT 'cp '|| NAME ||' /home/oracle/backup' FROM v$controlfile;
```

(未完)




