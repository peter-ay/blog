---
title: oracle启动报错ORA-03113 end-of-file on communication channel
date: 2020-05-28 17:30:37
tags: oracle 12c
categories: 
- [读书笔记,oracle]
---

1. 启动到mount状态下：

```sql
sqlplus /nolog:
--进入到sql命令窗口下：
conn /as sysdba
startup mount; 
```

2. 查看恢复区（闪回区）位置及大小:

```sql
SQL> show parameter db_recovery;
```
 
3. 查询当前的使用状态

```sql
select file_type,PERCENT_SPACE_USED,NUMBER_OF_FILES from v$flash_recovery_area_usage;
```

4. 物理清除归档路径下的日志文件：

```sh
退出sql命令窗口：
cd /u01/flash_recovery_area/ORCL/archivelog --进入到对应的归档实例日志目录
rm -rf 
```
<!-- more -->
5. 进入rman命令行：通过rman管理工具清理。


连接上数据库实例：

```sh
rman----> connect target sys/sys_passwd
crosscheck backup; 
delete obsolete; 
delete expired backup; 
crosscheck archivelog all; 
delete expired archivelog all;
-- 退出rman 重新连接数据库
```

6.  删除完毕后查看结果：

```sql
sqlplus /nolog
SQL> connect /as sysdba
SQL> select * from V$FLASH_RECOVERY_AREA_USAGE;
```

7. 重新打开数据库


