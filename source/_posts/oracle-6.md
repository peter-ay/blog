---
title: oracle数据库用户和权限
date: 2020-12-2 14:56:28
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 查看角色所具有的系统权限

```sql
SELECT * FROM DBA_SYS_PRIVS dsp WHERE dsp.GRANTEE ='RESOURCE';
```

![1](oracle-6/1.png)

## 查看系统中总共有多少个角色

```sql
SELECT * FROM DBA_ROLES dr ;
```

![2](oracle-6/2.png)
<!-- more -->
## 查看一个角色所具有的系统权限

``` sql
SELECT * FROM ROLE_SYS_PRIVS rsp WHERE rsp."ROLE" ='CONNECT';
```

![3](oracle-6/3.png)

## 查看一个角色所具有的对象权限

```sql
SELECT * FROM DBA_TAB_PRIVS rtp WHERE rtp.GRANTEE ='PUBLIC';
```

![4](oracle-6/4.png)

## 查看一个用户所具有的系统权限

```sql
SELECT * FROM DBA_SYS_PRIVS  dsp WHERE dsp.GRANTEE ='SCOTT'
```

![4](oracle-6/5.png)



