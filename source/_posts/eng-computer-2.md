---
title: 计算机专业英语教程第2版 第1期:四类计算机
date: 2021-02-21 21:06:56
tags:
- english
categories:
- [英语学习,计算机专业英语]
---

## 正文

* Computers are electronic devices that can follow instructions to accept input, process that input, and produce information. There are four types of computers: microcomputers, minicomputers, mainframe computers, and supercomputers.
计算机是根据指令接收输入，处理输入数据并产生信息的电子设备。有四种类型的计算机：微型机、小型机、大型机和巨型机。

* Microcomputers, also known as personal computers, are small computers that can fit on a desktop.Portable microcomputers can fit in a briefcase or even in the palm of your hand. Microcomputers are used in homes, schools, and industry. Today nearly every field uses microcomputers.
微型计算机，亦被称为个人计算机，是可以放在桌面上的小的计算机。便携式微型机可以放入手提箱，甚至手掌中。微型机被用于家庭、学校及工业当中。如今几乎每一领域都在使用微型机。

* One type of microcomputer that is rapidly growing in popularity is the portable computer, which can be easily carried around. There are four categories of portable computers.
正在迅速普及的一种微型机是便携式计算机，很易于四处携带。有四种类型的便携式计算机。

* Laptops: laptops, which weigh between 10 and 16 pounds, may be AC-powered, battery-powered,or both.The AC-powered laptop weighs 12 to 16 pounds. The battery-powered laptop weighs 10 to 15pounds, batteries included, and can be carried on a shoulder strap.
膝上电脑：其重量在10~16磅之间，可以是交流供电，电池供电或两者均可。交流供电的膝上电脑重量在12~16磅之间。电池供电的膝上电脑的重量包括电池在内是10~15磅之间，可以用肩带背起来携带。

* Notebook PCs: notebook personal computers weigh between 5 and 10 pounds and can fit into most briefcases.It is especially valuable in locations where electrical connections are not available.Notebook computers are the most popular portable computers today.
笔记本个人电脑：其重量在5~10磅之间，并且可放入大多数公文包中，它主要是用于连接电源不方便的地方。笔记本电脑是如今最流行的便携式电脑。

* Subnotebooks: subnotebooks are for frequent flyers and life-on-the-road types.Subnotebooks users give up a full-size display screen and keyboard in exchange for less weight.Weighting between 2 and 6 pounds, these computer fit easily into a briefcase.
超轻薄笔记本电脑：用于经常出差在路上的一类人。超轻薄笔记本电脑用户为了较轻的重量放弃了完整的显示屏幕和键盘的标准尺寸，但换来的是重量较轻。其重量在2~ 6磅之间，可以很容易地放入公文包中。

* Personal Digital Assistants: much smaller than even the subnotebooks.Personal Digital Assistants (PDAs) weigh from 1 to 4 pounds.The typical PDA combines pen input, writing recognition, personal organizational tools, and communication capabilities in a very small package.
个人数字助手：比超轻薄笔记本电脑还要小得多，其重量在1~ 4磅之间。典型的个人数字助手将钢笔输入、书写识别、个人编排工具和通信功能结合起来放入小包中。

<!-- more -->

* Minicomputers, also knows as midrange computers, are desk-sized machines.They fall into between microcomputers and mainframes in their processing speeds and data-storing capacities. Medium-size companies or departments of large companies typically use them for specific purposes.For example, they might use them to do research or to monitor a particular manufacturing process.Smaller-size companies typically use microcomputers for their general data processing needs, such as accounting.
小型计算机，也被称为中型机，是像书桌大小的机器。它们的处理速度和数据存储能力介于微型机和大型机之间。中型公司或大型公司的部门一般使用它们用于特殊用途。例如，可以使用它们作研究或监视某一个生产过程。小型公司一股使用小型机进行总的数据处理，比如说统计。

* Mainframe computers are larger computers occupying specially wired, air-conditioned rooms and capable of great processing speeds and data storage. They are used by large organizations business, banks, universities, government agencies—to handle millions of transactions. For example, insurance companies use mainframes to process information about millions of policyholders.
大型机是较大的计算机，放置在具有专线、空调的房间中，能够具有很快的处理速度和很大的数据存储量。它们通常是由一些大的组织机构使用——商业部门、银行、大学、政府机构——以处理数以百万计的事务。例如，保险公司使用大型机以处理数以百万计的保险客户的信息。

* Supercomputers are special, high-capacity computers used by very large organizations principally for research purposes. Among their uses are oil exploration and worldwide weather forecasting.
巨型机是由非常大的机构用于研究的大容盘专用计算机。在这些应用当中包括石油勘探和世界范围的天气预报。 In general, a computer's type is determined by the following

* seven factors: 一般说来，计算机的类型是由下列7个因素决定的：

* The type of CPU. Microcomputers use microprocessors. The larger computers tend to use CPUs made up of separate, high-speed, sophisticated components.
  CPU的类型。微型计算机使用微处理器。较大计算机趋向于使用由分开的髙速复杂的零部件构成的CPU。
  
* The amount of main memory the CPU can use. A computer equipped with a large amount of main memory can support more sophisticated programs and can even hold several different programs in memory at the same time.
CPU能够使用的内存的总量。配备有大容量内存的计算机可以支持更复杂的程序，并且能同时容纳几个不同的程序。

* The capacity of the storage devices. The larger computers systems tend to be equipped with higher capacity storage devices. The speed of the output devices.
存储设备的容量，较大计算机系统趋向于配置较大容盘的存储设备。

* The speed of microcomputer output devices tends to be rated in terms of the number of characters per second (cps) that can be printed usually in tens and hundreds of cps. Larger computers' output devices are faster and are usually rated at speeds of hundreds or thousands of lines that can be printed per minute.
输出设备的速度。微机输出设备的速度趋向于用每秒钟能打印的字符数（cps）予以度量。通常每秒为几十个、几百个字符。较大计算机的输出设备的速度也较快，通常每分钟可打印几百或几千行。

* The processing speed in millions of instructions per second (mips). The term instruction is used here to describe a basic task the software asks the computer to perform while also identifying the data to be affected. The processing speed of the smaller computers ranges from 7 to 40 mips. The speed of large computers can be 30 to 150 mips or more, and supercomputers can process more than 200 mips. In other words, a mainframe computer can process your data a great deal faster than a microcomputer can.
用mips (每秒钟百万条指令）度量处理速度。在这里用术语“指令”来描述软件要求计算机完成的基本任务，并且标识受到影响的数据。较小计算机的处理速度为7~ 40mips或更多。大型计算机的处理速度能达到30~500mips或更多。巨型计算机能处理200多mips。换句话说，大型计算机处理数据的能力要比微机快得多。

* The number of users that can access the computer at one time. Most small computers can support only a single user, some can support as many as two or three at a time. Large computers can support hundreds of users simultaneously.
一次可以访问计算机的用户数量。大多数小型计算机只能支持单个用户，有些计算机—次可以支持两个或三个用户，大型计算机则同时可由几百个用户使用。

* The cost of the computer system. Business systems can cost as little as $500 (for a microcomputer) or as much as $10 million (for a mainframe)and much more for supercomputer.
计算机系统的价格，商用计算机系统的价格从只值500美元(一台微机〉到要开支1，000 万美元（一台大型机），巨型计算机则花费更多。
