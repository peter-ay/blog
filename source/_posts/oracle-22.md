---
title: oracle数据装载
date: 2021-02-19 00:01:27
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## SQL*LOADER

* sqlldr help=y 查看帮助
* 建立控制文件

```txt
load data
infile '11.txt'
into table address insert
(
sno char terminated by ',',
zz char terminated by ','
)
```

```sh
sqlldr userid=scott.scott@orcl control=11.ctl log=11.log
```

## 操作类型

* insert --缺省方式，数据装载开始时要求表为空
* append --表中追加
* replace --删除旧记录替换成新装载记录(delete from table)
* truncate --删除旧记录替换成新装载记录(truncate table)

```txt
load data
infile '11.txt'
into table address insert
fields terminated by ','
(
sno  ,
zz  
)
```

## 导出数据

```sql
# 在sqlplus中
spool '/tmp/1.txt'
select empno || ',' || ename || ',' || to_char(hiredate,'yyyy-mm-dd') from emp;
spool off
```
<!-- more -->

## 外部表

* 外部表中的数据不装入数据库中，数据库只存储外部表的定义信息，实际的数据位于操作系统的平面文件中，但是在数据库中可以像访问正常表一样，通过sleet语句来访问平面文件中的数据
* 外部表是只读的
* 可以使用SQL，PL/SQL,JAVA访问外部表
* 外部表分为两种：使用数据泵引擎生成的外部表；根据文本文件创建的外部表

```sql
create table dept_ext (new_deptno,new_dname,new_loc)
ORGANIZATION EXTERNAL
(
TYPE oracle_datapump
DEFAULT DIRECTORY my_dir
LOCATION ('dept1.dmp','dept.dmp')
)
PARALLEL
AS
select deptno,dname,loc from dept;
```
