---
title: oracle逻辑备份
date: 2021-02-18 10:12:15
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 备份是数据库的数据副本，它可以保护数据在出现意外损失时最大限度的恢复
  * 物理备份: 是对数据库的操作系统物理文件（如数据文件、控制文件和日志文件等）的备份
  * 逻辑备份: 是对数据库的逻辑组件（如表、视图和存储过程等数据对象）的备份

## 数据库操作终止的四种故障

* 语句故障：执行SQL语句无效导致
* 用户进程故障: 用户进程出错导致无法访问数据库时发生用户进程故障。原因是异常断开连接或异常终止进程
* 实例故障: 实例由于硬件或软件无法继续运行时，发生实例故障
* 介质故障: 在数据库无法正确读取或写入某个数据库文件时，发生介质故障

## 传统的导入导出实用程序

* 是客户端工具
* 用于实施数据库的逻辑备份和恢复
* 导出程序将数据库中的对象定义和数据备份到一个操作系统的二进制文件中
* 导入程序读取二进制导出文件并将对象和数据载入数据库中

### 特点

* 可以按时间保存标结构和数据
* 允许导出指定的表，并重新导入到新的数据库中
* 可以把数据库迁移到另外一台异构服务器上
* 在两个不同版本的oracle数据库直接传输数据
* 在联机状态下进行备份和恢复
* 可以重新组织表的存储结构，减少连接及磁盘碎片

#### 四种模式

* 数据库模式
* 表空间模式
* 用户模式
* 表模式

<!-- more -->

## 例子

* 可以用exp help=y 查看帮助

```sh
#导出
#按表方式
exp scott/scott@orcl tables=(emp,dept) file=/tmp/1.dmp log=/tmp/1.log
#按用户方式
exp scott/scott@orcl owner=scott file=/tmp/1.dmp log=/tmp/1.log
#按表方式
exp scott/scott@orcl tablespaces=users file=/tmp/1.dmp log=/tmp/1.log
#使用参数文件方式
exp scott/scott@orcl parfile='/tmp/para1.txt'
```

```sh
#导入,可以指定表名，选择性导入
#步骤是先创建，后插入
imp scott/scott@orcl tables=(emp,dept) file=/tmp/1.dmp
imp scott/scott@orcl file=/tmp/1.dmp fromuser=scott touser=user1 #不可以
imp user1/user1@orcl file=/tmp/1.dmp fromuser=scott touser=user1 #可以
imp sys/sys@orcl file=/tmp/1.dmp fromuser=scott touser=user1 #可以
```

### 可传输表空间

* 如果迁移的数据量很大，可以使用可传输表空间
* dbms_tts 程序包检查表空间是否自包含
* 将表空间设置为只读
* exp进行可传输表空间模式的导出
* 将导出文件和数据文件复制到目标数据库上
* 目标数据库上，Imp进行可传输表空间模式的导入
* 目标数据库上，把表空间设置读写状态

```sh
# 导出
alter tablespace tab1 read only;
exp system/system@orcl tablespaces=tab1 transport_tablespace=y file=/tmp/1.dmp
```

```sh
# 导入
imp system/system@orcl1 tablespaces=tab1 transport_tablespace=y file=/tmp/1.dmp datafiles='/u01/datafile/TB01.DBF'
alter tablespace tab1 read write;
```

## 11g中数据泵

* exp/imp的缺点是速度太慢，在大型生产库中尤为明显。oracle设计了数据泵，这是一个服务器工具，它为oracle数据提供高速并行及大数据的迁移。
* imp/exp可以在客户端调用，但是expdp/impdp只能在服务端，因为在使用expdp/impdp以前需要在数据库中创建一个directory
* 在expdb进行导出时，先创建了MT表，并把对象的信息插入到MT表，之后进行导出动作；导出完成后，MT表也导出到转储文件中；导出任务完成后，或者删除了导出任务后，MT表自动删除；如果导出任务异常终止，MT表一日保留。
* expdp也具有四种模式:表，用户，可传输表空间，全库

```sql
# 建立目录对象
create directory MY_DIR as '/tmp/dir1';
grant read,write on directory MY_DIR to scottt;
```

```sh
# 
expdp scott/scott@orcl directory=MY_DIR dumpfile=expdp_scott1.dmp tables=(student,address)
# 导出scott、hr用户的内容:
expdp system/system@orcl1 directory=MY_DIR dumpfile=expdp_systme1.dmp schemas=(scott,hr) job_name=expdp1 #指定MT表的名字
```
