---
title: oracle触发器
date: 2021-02-08 15:42:48
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 当特定的事件出现时自动执行的存储过程
* 特定事件可以是执行更新的DML(增删改)和DDL
* 触发器不能显式调用
* 触发器的功能
  * 自动生成数据
  * 自定义复杂的安全权限
  * 提供审计和日志记录
  * 启用复杂的业务逻辑

## 语法

```sql
CREATE [OR REPLACE] TRIGGER
 trigger_name
AFTER | BEFORE | INSTEAD OF
[INSERT] [[OR] UPDATE [OF column_list]]
[[OR] DELETE]
ON table_or_view_name
[REFERENCING {OLD [AS] old /NEW [AS] new}]
[FOR EACH ROW]
[WHEN (condition)]
pl/sql_block;
```

```sql
--没有or delete 触发器对delete动作无效
create or replace trigger trig1 before insert or update on emp
begin
    dbms_output.put_line('触发器trig1响应了');
end
```

* 加for each row 变成行级触发器
<!-- more -->

## 例子

* 判断用户输入是否少于0

```sql
create or replace trigger trig1 before insert on emp for each row --一定要有for each row指定是行级触发器
begin
 if :new.sno<0 then
  --ROLLBACK不能用在触发器，COMMIT，create，drop，alter，savepoint
  raise_application_error(-2000,'不能插入负数');
 end if;
end
```

对于before触发器:

* 当执行insert的时候，:new存在，:old没有
* 当执行delete的时候，:new没有，:old存在
* 当执行update的时候, ：new存在，:old存在(先删除，后插入)

## instead of 触发器

* 更新非键保留表的视图

```sql
create or replace trigger tri_view instead of update on view_stu_add for each row
deaclare
 aa number:=0
begin
 select sno into aa from student where sname=:old.sname;
 delete address where sno=aa;
 insert into address values(aa,:new.zz);
end
```

## 记录增删改动作

```sql
create or replace trigger trig3 before insert or update or delete on emp for each row
begin
 if inserting then
  ...
 end if
 if updating then
  ...
 end if
 if deleting then
  ...
 end if
end
```

## 模式触发器

```sql
create or replace trigger log_drop_obj
after drop on schema
begin
 ...
end
```

## 常用的系统变量

* Ora_client_ip_address 返回客户端的ip地址
* Ora_database_name 返回当前数据库名
* Ora_login_user 返回登录用户名
* Ora_dict_obj_name 返回ddl操作所对应的数据库对象名
* Ora_dict_obj_type 返回ddl操作所对应的数据库对象的类型

## 数据库触发器

```sql
create or replace trigger tr_startup
after startup on database
begin
 ...
end
```

## 用户登录退出触发器

```sql
create or replace trigger tr_startup
after logon on database
begin
 ...
end
```

## 查看有关触发器的信息

* USER_TRIGGERS数据字典
