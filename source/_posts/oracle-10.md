---
title: oracle序列
date: 2021-01-27 10:11:04
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 定义

* 用于生产唯一、连续序号的对象
* 可以升序，可以降序
* CREATE SEQUENCE 创建序列

```sql
CREATE SEQUENCE s1
START WITH 1
INCREMENT BY 1
MAXVALUE 2000
MINVALUE 1
NOCYCLE -- 达到最大值后停止生成下一个值
CACHE 10; --指定内存中预先生成的序列数
```
<!-- more -->

## 查看用户的序列

![1](oracle-10/1.png)

![2](oracle-10/2.png)

![3](oracle-10/3.png)

## 取值

* 第一次取值必须用NEXTVUL

![4](oracle-10/4.png)

## 测试

![5](oracle-10/5.png)

## 应用

* 生成有规则的序列，例如2021_1 2021_2... 只要直接拼接就可以 '2021_' || to_char(s1.nextvul)

## 修改序列

* 使用ALTER SEQUENCE语句修改序列，不能修改序列的START WITH参数

```s1l
ALTER SEQUENCE s1 MAXVALUE 5000 CYCLE;
```

## 删除序列

```s1l
SROP SEQUENCE s1;
```
