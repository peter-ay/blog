---
title: oracle的表分区
date: 2021-01-26 10:35:15
tags: oracle 11g
categories: 
- [读书笔记,oracle]
---

## 表分区的概念

* 允许用户将一个表分成多个区
* 用户可以只查询特定分区
* 将不同分区存储在不同的磁盘，提高访问性能和安全性
* 可以独立地备份和恢复每个分区

## 分区方法

* 可以使用**user_tab_partitions**查看分区信息

### 范围分区

* 以表中的一个列或一组列的值的范围分区

```sql
PARTITION BY RANGE(column_name)
(
  PARTITION p1 VALUES LESS THAN(range1),
  PARTITION p1 VALUES LESS THAN(range1),
  ...
  [PARTITION p1 VALUES LESS THAN(MAXVALUE)]
);
```
<!-- more -->
### 散列分区

* 允许用户对不具有逻辑范围的数据进行分区
* 通过在分区键上执行HASH函数决定存储分区
* 将数据平均地分布到不同的分区

```sql
PARTITION BY HASH(column_name)
PARTITIONS number_of_partitions;
或
PARTITION BY HASH(column_name)
(
  PARTITION p1 [TABLESPACE tbs1],
  PARTITION p2 [TABLESPACE tbs2],
  ...
  [PARTITION pn [TABLESPACE tbsN]
);
```

### 列表分区

* 允许用户将不相关的数据组织在一起

```sql
PARTITION BY LIST(column_name)
(
  PARTITION p1 VALUES  (values_list1),
  PARTITION p2 VALUES  (values_list2),
  ...
  [PARTITION pn VALUES (DEFAULT)]
);
```

### 复合分区

* 范围分区与散列分区或列表分区的组合

```sql
PARTITION BY RANGE(column_name1)
SUBPARTITION BY HASH(column_name2)
SUBPARTITIONS number_of_partitions
(
  PARTITION p1 VALUES LESS THAN(range1),
  PARTITION p1 VALUES LESS THAN(range1),
  ...
  [PARTITION p1 VALUES LESS THAN(MAXVALUE)]
);
```

### 引用分区

* 基于外键引用的父表的分区方法，依赖父表字表的关系，子表通过外键关联到父表，继成父表的分区方式和维护操作

### 虚拟列分区

* 把分区建立在某个虚拟列上，即建立在函数或者表达式计算结果上，来完成某种任务
