---
title: 计算机专业英语教程第2版 第14期:关系数据库的结构
date: 2021-02-21 19:51:26
tags:
- english
categories:
- [英语学习,计算机专业英语]
---

## 正文

* The relational model is the basis for any relational database management system (RDBMS). A relational model has three core components: a collection of objects or relations, operators that act on the objects or relations, and data integrity methods. In other words, it has a place to store the data, a way to create and retrieve the data, and a way to make sure that the data is logically consistent.
关系模型是任何关系数据库管理系统（RDBMS)的基础。一个关系模型有个核心组件：对象或关系的集合，作用于对象或关系上的操作，以及数据完整性规则。换均话说，关系数据库有一个存储数据的地方，一种创建和检索数据的方法，以及一种确认数据的逻辑一致性的方法。

* A relational database uses relations, or two-dimensional tables, to store the information needed to support a business. Let's go over the basic components of a traditional relational database system and look at how a relational database is designed. Once you have a solid understanding of what rows, columns, tables, and relationships are, you'll be well on your way to leveraging the power of a relational database.
一个关系数据库使用关系或—维表来存储支持某个事物所需的信息。让我们了解一下一个传统的关系数据库系统的基本组件并且学习如何设计一个关系数据库。一旦你对于行、列、表和关联是什么有了深刻理解，你就能够充分发挥关系数据库的强大功能。

* A table in a relational database, alternatively known as a relation, is a two-dimensional structure used to hold related information. A database consists of one or more related tables.
在关系数据库中，一个表（或者说一个关系）是一个用于保存相关信息的一维结构。一个数据库由一个或者多个相关联的表组成。

* Note: Don't confuse a relation with relationships. A relation is essentially a table, and a relationship is a way to correlate, join, or associate two tables.
注意：不要混淆了关系和关联。一个关系实际上是一个表，而一个关联指的是一种连接、结合或联合两个表的方式。

<!-- more -->

* A row in a table is a collection or instance of one thing, such as one employee or one line item on an invoice. A column contains all the information of a single type, and the piece of data at the intersection of a row and a column, a field, is the smallest piece of information that can be retrieved with the database's query language. For example, a table with information about employees might have a column called LAST_NAME that contains all of the employees' last names. Data is retrieved from a table by filtering on both the row and the column.
表中的一行是一种事物的集合或实例，比如一个员工或发票上的一项。表中的一列包含了一类信息：而且行列交义点上的数据，字段，即是能够用数据库查询语言检索到的最小片信息。举个例子来说，一个员工信息表可能有一个“名字”列，列中就包含所有员工的名字。数据是通过对行、列进行过滤而从表中检索出来的。

* Primary Keys, Datatypes, and Foreign Keys
主码、数据类型和外码

* The examples throughout this article will focus on the hypothetical work of Scott Smith, database developer and entrepreneur. He just started a new widget company and wants to implement a few of the basic business functions using the relational database to manage his Human Resources (HR) department.
本篇文章均以假设的斯科特•史密斯的工厂为例，他是数据库的建立者和企业的主办人。他刚开办了一个饰品公司并且想要使用关系数据库的几项基木功能来管理人力资源部门。

* Relation: A two-dimensional structure used to hold related information, also known as a table.
关系：用来保存相关信息的一个一维结构，也就是表。

* Note: Most of Scott's employees were hired away from one of his previous employers, some of whom have over 20 years of experience in the field. As a hiring incentive, Scott has agreed to keep the new employees' original hire date in the new database.
注意：大多数斯科特的雇员都是雇自过去的从业者，他们中有些人在这个领域己经有20年的经验了。出于雇用的目的，斯科特同意在新数据库中维持新进员工最初的雇佣日期。

* Row: A group of one or more data elements in a database table that describes a person, place, or thing.
行：在一个数据库表中的一组中单数据或多数据元素，用于描述一个人、地方或事物。

* Column: The component of a database table that contains all of the data of the same name and type across all rows.
列：列是数据库表的组件，它包含所存行中同名和同类型的所存数据。

* You'll learn about database design in the following sections, but let's assume for the moment that the majority of the database design is completed and some tables need to be implemented. Scott creates the EMP table to hold the basic employee information, and it looks something like this:
你会在下面章节学到如何设计数据库，现在让我们假设数据库大部分已经设计完成并且有一些表需要被执行。斯科特创建了EMP表来保存基本的员工信息,就像这样:

![1](eng-computer-1/1.png)

![2](eng-computer-1/2.png)

* Notice that some fields in the Commission (COMM) and Manager (MGR) columns do not contain a value; they are blank. A relational database can enforce the rule that fields in a column may or may not be empty. In this case, it makes sense for an employee who is not in the Sales department to have a blank Commission field. It also makes sense for the president of the company to have a blank Manager field, since that employee doesn't report to anyone.
你可能注意到佣金列和管理人列中有一些单元格中没有值：它们是空值。一个关系数据库能够规定列中的一个中元格是否为空。如此，可以明确那些非销售部的员工佣金单元为空。同样也明确了公司董事长的管理人单元为空，因为这个员工不需要向任何人汇报工作。

* Field: The smallest piece of information that can be retrieved by the database query language. A field is found at the intersection of a row and a column in a database table.
单元格：是数据库查询语言所能够检索到的最小片信息。一个单元格就是一个数据库表的行和列交叉形成的。

* On the other hand, none of the fields in the Employee Number (EMPNO) column are blank. The company always wants to assign an employee number to an employee, and that number must be different for each employee. One of the features of a relational database is that it can ensure that a value is entered into this column and that it is unique. The EMPNO column, in this case, is the primary key of the table.
另一方面，没有哪个员工的员工编号单元为空。公司总是希望为每个员工分配一个员工号，并且这个号码必须是每个员工都不同的。关系数据库的一个特性能够确定某列的键入值必须为单值。如此，员工编号列便是这个表的主码。

* Primary Key: A column (or columns) in a table that makes the row in the table
distinguishable from every other row in the same table.
主码：主码即是表中的一列（或多列），使每一行能够区别于同表中的其他行。

* Notice the different datatypes that are stored in the EMP table: numeric values, character or alphabetic values, and date values.
留意一下EMP表中存储的不同数据类型：数值型，字符型或字母型，以及日期型。

* As you might suspect, the DEPTNO column contains the department number for the employee. But how do you know what department name is associated with what number? Scott created the DEPT table to hold the descriptions for the department codes in the EMP table.
如你所想，部门成员列保存的是所在部门的编号。但是你如何知道哪个部门名称对成哪个部门编号呢？斯科特建立了DEPT表来具体描述EMP表中提到的部门编号的情况。

![3](eng-computer-1/3.png)

* The DEPTNO column in the EMP table contains the same values as the DEPTNO column in the DEPT table. In this case, the DEPTNO column in the EMP table is considered a foreign key to the same column in the DEPT table.
EMP表中的部门编号列同DEPT表中的部门编号列有着相同的值。既然如此，EMP表中的部门编号列便被看作是与DEPT表中相同列对成的外码。

* A foreign key enforces the concept of referential integrity in a relational database. The concept of referential integrity not only prevents an invalid department number from being inserted into the EMP table, but it also prevents a row in the DEPT table from being deleted if there are employees still assigned to that department.
外码加强了关系数据库中参考完整性的概念。参考完整性的概念不只可以阻止无效的部门编号被插入EMP表中，而且在某部门仍有员工的情况下，可以防止DEPT表中该部门的信息被刪除。

* Foreign Key: A column (or columns) in a table that draws its values from a primary or unique key column in another table. A foreign key assists in ensuring the data integrity of a table.
外码：表中的一列（或多列），它的值来自于其他表的主码列或中值列。一个外码有助于确定表中数据的完整性。

* Referential Integrity: A method employed by a relational database system that enforces one-to-many relationships between tables.
参考完整性：是关系数据库用来加强表间一对多关联的一种方式。

*Data Modeling
数据建模

* Before Scott created the actual tables in the database, he went through a design process known as data modeling. In this process, the developer conceptualizes and documents all the tables for the database. One of the common methods for modeling a database is called ERA, which stands for entities, relationships, and attributes. The database designer uses an application that can maintain entities, their attributes, and their relationships. In general, an entity corresponds to a table in the database, and the attributes of the entity correspond to columns of the table.
在斯科特于数据库中创建真实表之前，他要经过一个称作数据建模的过程。在这个过程中数据库创建者定义和填写数据库中所有表。有一种为数据库建模的方式叫作ERA，它可以表示出实体、实体间的关联和实体的属性。数据库设计者使用一个能够支持实体、实体属性和实体间关联的成用程序。通常，一个实体对成数据库中的一个表，而实体的属性对成于表中的列。

* Data Modeling: A process of defining the entities, attributes, and relationships between the entities in preparation for creating the physical database.
数据建模：一个定义实体、实休体属性和实体间关联的过程，从而为建立物理数据库做准备。

* The data-modeling process involves defining the entities, defining the relationships between those entities, and then defining the attributes for each of the entities. Once a cycle is complete, it is repeated as many times as necessary to ensure that the designer is capturing what is important enough to go into the database. Let's take a closer look at each step in the data-modeling process.
数据建模过程包括定义实体、定义实体间关联以及定义每个实体的属性的过程。一旦一个周期完成，就需要不断重复直到设计者抓住了重点，足以开始建立数据库。让我们进一步了解为数据库建模过程的步骤。

* Defining the Entities
定义实体

* First, the designer identifies all of the entities within the scope of the database application. The entities are the persons, places, or things that are important to the organization and need to be tracked in the database. Entities will most likely translate neatly to database tables. For example, for the first version of Scott's widget company database, he identifies four entities: employees, departments, salary grades, and bonuses. These will become the EMP, DEPT, SALGRADE, and BONUS tables.
首先，设计者确定数据库成用程序范围内的所有实体。实体是人、地方或事物，它们对于整个团体是重要的且需要被记录在数据库中。实体将被巧妙的转化为数据表。比如，在第一版斯科特饰品公司数据库中，他定义了四个实体：员工、部门、工资水平和奖金。 它们将称为EMP (员工)表，DEPT (部门）表，SALGRADE (工资水平）表和BONUS (奖金）表。

* Defining the Relationships Between Entities
定义实体间的关联

* Once the entities are defined, the designer can proceed with defining how each of the entities is related. Often, the designer will pair each entity with every other entity and ask, "Is there a relationship between these two entities?" Some relationships are obvious; some are not.
一旦定义了实体，设计者就能够继续定义每个实体间是如何关联的。通常，设计者通常将每个实体同其他实体配对，并且考虑：“两者之间是否存在关联呢？”实体间的某些关联是明显的，某些不是。

* In the widget company database, there is most likely a relationship between EMP and DEPT, but depending on the business rules, it is unlikely that the DEPT and SALGRADE entities are related. If the business rules were to restrict certain salary grades to certain departments, there would most likely be a new entity that defines the relationship between salary grades and departments. This entity would be known as an associative or intersection table and would contain the valid combinations of salary grades and departments.
在饰品公司数据库中，员工实体和部门实体间极可能存在关联，而依据事物间的关系原则，部门实体跟工资水平实体间似乎就没有关联了。如果事物间的关系原则是用来约束某个部门的工资水平的，就可能需要一个新的实体来说明工资水平和部门之间的关联。这个实体被称作关系表或交表，其中包含工资水平和部门之间的有效联合。

* Associative Table: A database table that stores the valid combinations of rows from two other tables and usually enforces a business rule. An associative table resolves a many-to-many relationship.
关系表：是一个数据库表，其中保存着另外两个表的行（记录）间的有效结合，并且通常强调了事物间的关系原则。关联表处理的是一个多对多关联。
In general, there are three types of relationships in a relational database:
通常，关系数据库间有三种关联方式：

* One-to-many: The most common type of relationship is one-to-many. This means that for each occurrence in a given entity, the parent entity, there may be one or more occurrences in a second entity, the child entity, to which it is related. For example, in the widget company database, the DEPT entity is a parent entity, and for each department, there could be one or more employees associated with that department. The relationship between DEPT and EMP is one-to-many.
一对多关联: 最常见的关联是一对多关联。意思是对于每个给出的现有实体（即父实体）都有一个或多个现有的另一个实体（即实体）与之相关联。举个例子来说，在饰品公司数据库中，部门实体是一个父实体，而每个部门中，都有一个或多个员工属于该部门。这样，部门实体和员工实体间的关联就是一对多关联。

* One-to-one: In a one-to-one relationship, a row in a table is related to only one or none of the rows in a second table. This relationship type is often used for subtyping. For example, an EMPLOYEE table may hold the information common to all employees, while the FULLTIME, PARTTIME, and CONTRACTOR tables hold information unique to full-time employees, part-time employees, and contractors, respectively. These entities would be considered subtypes of an EMPLOYEE and maintain a one-to-one relationship with the EMPLOYEE table. These relationships are not as common as one-to-many relationships, because if one entity has an occurrence for a corresponding row in another entity, in most cases, the attributes from both entities should be in a single entity.
一对一关联: 在一个一对一关联中，表中的一行只关联另一个表中的一行甚至0行。这种关联类型通常用于子类型数据中。例如，一个员工表可能保存了所有员工的信息，而全职表、兼职表和承包人表则分别保存全职员工、兼职员工和承包人的信息。这些实体被认为是员工表的子表，并且同员工表维持一对一关联。这种关系不像一对多关联那么常见，因为如果一个实体与另一个实体总有对应行，在大多数情况下，两个实体中的属性只在一个实体内出现就可以了。

* Many-to-many: In a many-to-many relationship, one row of a table may be related to many rows of another table, and vice versa. Usually, when this relationship is implemented in the database, a third entity is defined as an intersection table to contain the associations between the two entities in the relationship. For example, in a database used for school class enrollment, the STUDENT table has a many-to-many relationship with the CLASS table—one student may take one or more classes, and a given class may have one or more students. The intersection table STUDENT_CLASS would contain the combinations of STUDENT and CLASS to track which students are in which classes.
多对多关联: 在多对多关联中，表的一行可能对成另一个表的许多行，反之亦然。通常，当这些关联在数据库中被执行时，往往再定义第三个实体用来保存前两个实体间的所有关联。例如，在一个学籍注册数据库中，学生表与班级表之间有一个多对多关联一个学生可能听一门或多门课程，并且一个班级也可能有一个或多个学生。而学生_班级关系表中就包含了学生和班级之间的关系，以表明哪个学生在哪个班。

* Assigning Attributes to Entities
指定实体属性

* Once the designer has defined the entity relationships, the next step is to assign the attributes to each entity. This is physically implemented using columns, as shown here for the SALGRADE table as derived from the salary grade entity.
一旦设计者定义了实体间关联，下一步就是去指定每个实体的属性。这是实现列的使用，如右图所示由工资水平实体所建立的工资水平表。

* Iterate the Process: Are We There Yet?
重复步骤：我们仍然在原地

* After the entities, relationships, and attributes have been defined, the designer may iterate the data modeling many more times. When reviewing relationships, new entities may be discovered. For example, when discussing the widget inventory table and its relationship to a customer order, the need for a shipping restrictions table may arise.
在定义了实体、关联以及属性之后，设计者往往要多重复几次数据建模过程。当我们在回顾关联时，就会发现需要建立新的实体。比如，当讨论饰品库存表和与它相关的客户订单时，就会发现需要制定一个送货约束表。

* Once the design process is complete, the physical database tables may be created. Logical database design sessions should not involve physical implementation issues, but once the design has gone through an iteration or two, it's the DBA's job to bring the designers "down to earth." As a result, the design may need to be revisited to balance the ideal database implementation versus the realities of budgets and schedules.
一旦设计过程完成，下面将要建立实际的数据库表。逻辑数据库的设计过程不会牵涉实际执行中的问题。然而，一旦设计进入到实际的运作，数据库存管理员就会很快让设计者从理想回到现实来。结果，设计就可能需要再次构想以求得理想的数据库运行与预算和进度之间的平衡。

![4](eng-computer-1/4.png)
