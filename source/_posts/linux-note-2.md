---
title: 文件系统属性chattr权限
date: 2020-04-07 10:56:24
tags:
- chattr
- linux
categories:
- [linux]
---

这个权限十分隐蔽，很容易忽视

## 命令格式

***chattr命令只能是root权限,lsattr则普通用户也可以使用***

> chattr [+-=] [选项] 文件或目录

+ i属性: 如果对文件设置了i属性，则不允许对文件进行删除、改名，也不能添加和修改数据；如果对目录设置了i属性，那么只能修改目录下文件的数据，但不允许建立和删除文件。
+ a属性: 只能对文件增加数据，不能删除和修改数据(注意不能用VIM操作，因为系统无法判断是增加还是修改)，如果对目录设置a属性，则只允许目录中建立和修改文件，不允许删除。
+ =属性: 等于某个权限。

测试文件：

```sh
touch abc
lsattr
# --------------e--- ./abc
sudo chattr +i abc
lsattr
# ----i---------e--- ./abc
echo 'test' > abc
# -bash: abc: Operation not permitted
rm -rf abc
# rm: cannot remove 'abc': Operation not permitted
sudo chattr -i abc
lsattr abc
# --------------e--- abc
rm -rf abc
```
<!-- more -->

测试目录

```sh
mkdir qqq
lsattr
# --------------e--- ./qqq
sudo chattr +i qqq
lsattr
# ----i---------e--- ./qqq
cd qqq
touch abc
#touch: setting times of 'abc': No such file or directory
```

***目录的attr显示有时需要需要用 lsattr -d，但是ubuntu下实测不加也可以显示***

***

## 总结

+ chattr可以用来解决误删除，但是实际使用中很隐蔽，很容易忽略。

***
