---
title: 日常口语1-退款
date: 2020-03-19 10:20:51
tags:
- english
- daily conversation
categories:
- [英语学习,日常会话]
---

### 鉴于...由于 In light of/In view of/Considering/Seeing

***
>A:Hello, I'd like to retuen this woolen sweater.  
>你好，我想退还这件羊毛衫。  
>
>B:Do you have the receipt?  
>你有收据吗?  
>
>A:Yes,it's in a box.  
>有，在盒子里
><!-- more -->
>B:Would you like to try on a larger size?  
>你想试穿大一号的吗？  
>
>A:No thanks,I just want my money back.  
>不了，谢谢，我只想把钱拿回来。
>
>B:We don't provide refunds.although i'll be happy to exchange your purchase.  
>我们不退款，不过我很乐意更换你的商品
>
>A:If that's the case,can I try on a different style,please?  
>若是如此，请问我可以试穿不同的款式吗？
>
>B:Items can only be exchange for those of the same style.  
>只有相同款式的商品才可以更换。
>
>A:I don't want the same style.Not only the arms too tight,but the body was too baggy,If I get a larger size,
the arms might be alright,but the body will be even looser.  
>我不想要相同的款式，不仅袖子太窄，而且衣身太宽松。如果我拿大一号的，袖子可能没有问题，但衣身会更跨。
>
>In light of the fact that your expectations were not met,I'll bend the rules and give you a full refund.  
>先生，由于你但期望落空，我就放宽规定，给你全额退款。
