---
title: "| 关于自己 |"
date: 2020-03-19 12:14:12
---

佛系博主，喜欢Go、Python、JS等，空闲时间弄一点web技术分享、学习心得、个人开发经历分享、源码分析等。在技术的路上我们一起成长~

公众号: Go时代

![1](/images/qr1.jpg)
